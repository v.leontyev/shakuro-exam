﻿using System.ComponentModel;

namespace TG.Exam.WebMVC.Enums
{
    /// <summary>
    /// Fetch method type
    /// </summary>
    public enum FetchMethodEnum
    {
        /// <summary>
        /// Sync
        /// </summary>
        [Description("Sync")]
        Sync = 0,

        /// <summary>
        /// Async (via AJAX)
        /// </summary>
        [Description("Async")]
        Async = 1
    }
}