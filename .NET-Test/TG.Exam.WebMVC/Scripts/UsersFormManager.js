﻿/**
 * User form manager
 * */

var UsersFormManager = function() {
    var _controls = {
        invokeButtonId: 'InvokeButton',
        tableId: 'UsersTable'
    }

    var _urls = {
        usersApi: '/api/users'
    }

    /**
     * Get array of users from web server
     */
    function getUsersData() {
        $.get(_urls.usersApi, function(data) {
            initGrid(data);
        });
    }

    /**
     * Initialize grid with data
     * @param {any} data Array of users
     */
    function initGrid(data) {
        if (data) {
            var tableBody = $("#" + _controls.tableId + " > tbody");
            tableBody.empty();

            data.forEach(function(item) {
                var row = '<tr>' +
                    '<td>' + item.FirstName + '</td>' +
                    '<td>' + item.LastName + '</td>' +
                    '<td>' + item.Age + '</td>' +
                    '<td>' + item.FetchMethod + '</td>' +
                    '</tr>';
                tableBody.append(row);
            });
        }
    }

    return {
        /**
         * Initialize form manager
         * */
        init: function() {
            $("#" + _controls.invokeButtonId).click(function() {
                getUsersData();
            });
        }
    }
}