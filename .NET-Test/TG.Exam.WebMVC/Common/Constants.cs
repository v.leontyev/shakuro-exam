﻿namespace TG.Exam.WebMVC.Common
{
    /// <summary>
    /// Constants class
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Zero years increase constant for <see cref="TG.Exam.WebMVC.Models.UserViewModel.Age"> value
        /// </summary>
        public const int UserViewModelAgeZeroYearsIncrease = 0;

        /// <summary>
        /// Ten years increase constant for <see cref="TG.Exam.WebMVC.Models.UserViewModel.Age"> value
        /// </summary>
        public const int UserViewModelAgeTenYearsIncrease = 10;
    }
}