﻿using TG.Exam.WebMVC.Enums;
using TG.Exam.WebMVC.Extensions;
using TG.Exam.WebMVC.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TG.Exam.WebMVC.Models
{
    /// <summary>
    /// User model
    /// </summary>
    public class UserViewModel
    {
        /// <summary>
        /// First name
        /// </summary>
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        /// <summary>
        /// Age
        /// </summary>
        [Display(Name = "Age")]
        public int Age { get; set; }

        /// <summary>
        /// Load table type
        /// </summary>
        [Display(Name = "Fetch method")]
        public string FetchMethod { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{FirstName} {LastName} is {Age} years old";
        }

        /// <summary>
        /// Get list of possible objects of class User
        /// </summary>
        /// <remarks>
        /// Sets fetch method type on <see cref="TG.Exam.WebMVC.Enums.FetchMethodEnum.Sync">
        /// </remarks>
        /// <returns>List of possible objects</returns>
        public static IList<UserViewModel> GetAllSyncMethod()
        {
            return GetAll(FetchMethodEnum.Sync, Constants.UserViewModelAgeZeroYearsIncrease);
        }

        /// <summary>
        /// Get list of possible objects of class User
        /// </summary>
        /// <remarks>
        /// Sets fetch method type on <see cref="TG.Exam.WebMVC.Enums.FetchMethodEnum.Async"> and increases <see cref="TG.Exam.WebMVC.Models.UserViewModel.Age"> value by 10
        /// </remarks>
        /// <returns>List of possible objects</returns>
        public static IList<UserViewModel> GetAllAsyncMethod()
        {
            return GetAll(FetchMethodEnum.Async, Constants.UserViewModelAgeTenYearsIncrease);
        }

        /// <summary>
        /// Get list of possible objects of class User
        /// </summary>
        /// <param name="fetchMethod">Fetch method type</param>
        /// <param name="ageIncrease">Age by which the value <see cref="TG.Exam.WebMVC.Models.UserViewModel.Age"> well be increased</param>
        /// <returns>List of UserViewModel objects</returns>
        public static IList<UserViewModel> GetAll(FetchMethodEnum fetchMethod, int ageIncrease)
        {
            var users = new List<UserViewModel> 
            {
                new UserViewModel
                {
                    Age = 35 + ageIncrease,
                    FirstName = "Dana",
                    LastName = "Cohen",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 34 + ageIncrease,
                    FirstName = "Michal",
                    LastName = "Yaari",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 32 + ageIncrease,
                    FirstName = "Eitan",
                    LastName = "Foxx",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 46 + ageIncrease,
                    FirstName = "Gilad",
                    LastName = "Sade",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 26 + ageIncrease,
                    FirstName = "Ariel",
                    LastName = "Binyamin",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 46 + ageIncrease,
                    FirstName = "Chen",
                    LastName = "Mizrahi",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 45 + ageIncrease,
                    FirstName = "Dorin",
                    LastName = "Yechzceli",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 19 + ageIncrease,
                    FirstName = "Benny",
                    LastName = "Binyamin",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 22 + ageIncrease,
                    FirstName = "Michal",
                    LastName = "Ar-Lev",
                    FetchMethod = fetchMethod.GetDescription(),
                },
                new UserViewModel
                {
                    Age = 31 + ageIncrease,
                    FirstName = "Guy",
                    LastName = "Dabush",
                    FetchMethod = fetchMethod.GetDescription(),
                },
            };

            return users;
        }
    }
}
