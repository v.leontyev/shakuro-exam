﻿using System.Web.Mvc;
using TG.Exam.WebMVC.Models;

namespace TG.Exam.WebMVC.Controllers
{
    /// <summary>
    /// Users MVC controller
    /// </summary>
    public class UsersController : Controller
    {
        /// <summary>
        /// Get list of user models with <see cref="TG.Exam.WebMVC.Enums.FetchMethodEnum.Sync"> and return index view
        /// </summary>
        /// <returns>Returns a ViewResult object</returns>
        public ActionResult Index()
        {
            var model = UserViewModel.GetAllSyncMethod();
            return View(model);
        }
    }
}