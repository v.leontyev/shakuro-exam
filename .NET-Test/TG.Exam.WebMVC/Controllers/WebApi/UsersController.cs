﻿using System.Collections.Generic;
using System.Web.Http;
using TG.Exam.WebMVC.Models;

namespace TG.Exam.WebMVC.Controllers.WebApi
{
    /// <summary>
    /// Users web api controller
    /// </summary>
    public class UsersController : ApiController
    {
        /// <summary>
        /// Get list of user models with <see cref="TG.Exam.WebMVC.Enums.FetchMethodEnum.Async"> and increase Age property value by 10
        /// </summary>
        /// <returns>List of user models</returns>
        public IList<UserViewModel> Get()
        {
            return UserViewModel.GetAllAsyncMethod();
        }
    }
}