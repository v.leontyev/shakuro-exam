﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace TG.Exam.WebMVC.Extensions
{
    /// <summary>
    /// Enum extensions
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Get <see cref="System.ComponentModel.DescriptionAttribute"> value of enum
        /// </summary>
        /// <param name="enum">Enum</param>
        /// <returns>Description</returns>
        public static string GetDescription(this Enum @enum)
        {
            if (@enum == null)
                return string.Empty;

            FieldInfo fi = @enum.GetType().GetField(@enum.ToString());
            DescriptionAttribute attribute = fi.GetCustomAttributes(typeof(DescriptionAttribute), false).Cast<DescriptionAttribute>().FirstOrDefault();

            return attribute != null 
                ? attribute.Description 
                : @enum.ToString();
        }
    }
}