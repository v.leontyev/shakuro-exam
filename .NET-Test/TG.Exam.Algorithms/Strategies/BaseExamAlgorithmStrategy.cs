﻿using System;
using TG.Exam.Algorithms.Interfaces;

namespace TG.Exam.Algorithms.Strategies
{
    /// <summary>
    /// Exam algorithm strategy base class
    /// </summary>
    public abstract class BaseExamAlgorithmStrategy : IExamAlgorithmStrategy
    {
        /// <summary>
        /// Generates fibonacci sequence and returns C-th element of the sequence
        /// </summary>
        /// <param name="a">First number</param>
        /// <param name="b">Second number</param>
        /// <param name="c">Depth</param>
        /// <returns>C-th element of the sequence</returns>
        protected abstract int Foo(int a, int b, int c);

        /// <summary>
        /// Array sorting algorithm
        /// </summary>
        /// <param name="arr">Array</param>
        /// <returns>Sorted array</returns>
        protected abstract int[] Bar(int[] arr);

        /// <inhertdoc />
        public void Execute()
        {
            Console.WriteLine($"{GetType().Name}");
            Console.WriteLine("Foo result: {0}", Foo(7, 2, 8));
            Console.WriteLine("Bar result: {0}", string.Join(", ", Bar(new[] { 7, 2, 8 })));
        }
    }
}