﻿namespace TG.Exam.Algorithms.Strategies
{
    /// <summary>
    /// My version of the exam task
    /// </summary>
    public class ExamAlgorithmMyVersionStrategy : BaseExamAlgorithmStrategy
    {
        /// <remarks>
        /// I propose the iterative variant of <see cref="ExamAlgorithmOriginalVersionStrategy.Foo(int, int, int)">
        /// It's better because this variant has no problems with stack.
        /// </remarks>
        protected override int Foo(int a, int b, int c)
        {
            int res = a;
            int prev = b;

            for (int i = 0; i < c - 1; i++)
            {
                int temp = res;
                res = prev;
                prev += temp;
            }
            return res;
        }

        /// <remarks>
        /// Bubble sort can be improved by adding "swapped" flag. 
        /// If during current iteration there were no swapps, we can claim array as sorted already.
        /// It still bad for worst case, but for average case it become more effecient.
        /// </remarks>
        protected override int[] Bar(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                var swapped = false;
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int t = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = t;
                        swapped = true;
                    }
                }

                if (!swapped)
                    break;
            }

            return arr;
        }
    }
}