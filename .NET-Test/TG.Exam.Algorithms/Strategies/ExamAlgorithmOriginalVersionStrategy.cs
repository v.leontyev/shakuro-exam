﻿namespace TG.Exam.Algorithms.Strategies
{
    /// <summary>
    /// Original version of the exam task
    /// </summary>
    public class ExamAlgorithmOriginalVersionStrategy : BaseExamAlgorithmStrategy
    {
        /// <remarks>
        /// 1) Find out what this method does.
        ///     Method implements an algorithm of generating fibonacci sequence and returns C-th element of the sequence.
        ///     The difference from the classic algorithm is that first two numbers are parameterized.
        ///     The method is recursive.
        ///  
        /// 2) Describe the advantages/disadvantages of such implementation as a comment in code.
        ///     Advantages:
        ///         - Solving problems using recursive method can be more easy than iterative one. The iterative method can be more big and complex.
        ///     
        ///     Disadvantages:
        ///         - It's more difficult to debug recursive methods rather than iterative variant;
        ///         - Recursion may cause StackOverflowException;
        ///         - As a result of the second point, there is limit for depth of the sequence parameter until the StackOverflowException will be thrown. 
        /// 
        /// 3) Implement better approach (if you see it).
        ///     <see cref="ExamAlgorithmMyVersionStrategy.Foo(int, int, int)">
        /// 
        /// </remarks>
        protected override int Foo(int a, int b, int c)
        {
            if (1 < c)
                return Foo(b, b + a, c - 1);
            else
                return a;
        }

        /// <remarks>
        /// 1) Find out what this method does.
        ///     Classic bubble sorting algorithm.
        /// 
        /// 2) Describe the advantages/disadvantages of such implementation as a comment in code.
        ///     Advantages:
        ///         - This algorithm is easy to understand and easy to implement;
        ///         - Performs greatly for the almost sorted array.
        /// 
        ///     Disadvantages:
        ///         - Very slow for large arrays because it compares all the pairs.
        ///         
        /// 3) Implement better approach (if you see it).
        ///     <see cref="ExamAlgorithmMyVersionStrategy.Bar(int[])">
        /// 
        /// </remarks>
        protected override int[] Bar(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                for (int j = 0; j < arr.Length - 1; j++)
                    if (arr[j] > arr[j + 1])
                    {
                        int t = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = t;
                    }
            return arr;
        }
    }
}