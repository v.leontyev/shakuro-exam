﻿namespace TG.Exam.Algorithms.Interfaces
{
    /// <summary>
    /// Exam algorithm strategy interface
    /// </summary>
    public interface IExamAlgorithmStrategy
    {
        /// <summary>
        /// Execute Foo&Bar methods
        /// </summary>
        void Execute();
    }
}