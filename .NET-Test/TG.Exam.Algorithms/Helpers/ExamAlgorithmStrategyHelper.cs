﻿using System;
using System.Collections.Generic;
using TG.Exam.Algorithms.Enums;
using TG.Exam.Algorithms.Interfaces;
using TG.Exam.Algorithms.Strategies;

namespace TG.Exam.Algorithms.Helpers
{
    /// <summary>
    /// Get strategy by enum value
    /// </summary>
    public static class ExamAlgorithmStrategyHelper
    {
        private static Dictionary<ExamAlgorithmEnum, IExamAlgorithmStrategy> _strategyDictionary = new Dictionary<ExamAlgorithmEnum, IExamAlgorithmStrategy>
        {
            { ExamAlgorithmEnum.OriginalVersion, new ExamAlgorithmOriginalVersionStrategy() },
            { ExamAlgorithmEnum.MyVersion, new ExamAlgorithmMyVersionStrategy() }
        };

        /// <summary>
        /// Get strategy by enum value
        /// </summary>
        /// <param name="algorithmEnum">Algorithm</param>
        /// <returns>Strategy</returns>
        public static IExamAlgorithmStrategy GetStrategy(ExamAlgorithmEnum algorithmEnum)
        {
            IExamAlgorithmStrategy strategy;
            if (_strategyDictionary.TryGetValue(algorithmEnum, out strategy))
                return strategy;
            else
                throw new NotSupportedException();

        }
    }
}
