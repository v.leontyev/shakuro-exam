﻿namespace TG.Exam.Algorithms.Enums
{
    public enum ExamAlgorithmEnum
    {
        /// <summary>
        /// Original version
        /// </summary>
        OriginalVersion = 0,

        /// <summary>
        /// MyVersion
        /// </summary>
        MyVersion = 1
    }
}