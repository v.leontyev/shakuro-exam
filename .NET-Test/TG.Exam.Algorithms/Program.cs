﻿using System;
using TG.Exam.Algorithms.Helpers;
using TG.Exam.Algorithms.Enums;

namespace TG.Exam.Algorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            var originalVersion = ExamAlgorithmStrategyHelper.GetStrategy(ExamAlgorithmEnum.OriginalVersion);
            var myVersion = ExamAlgorithmStrategyHelper.GetStrategy(ExamAlgorithmEnum.MyVersion);

            originalVersion.Execute();
            myVersion.Execute();

            Console.ReadKey();
        }
    }
}