﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TG.Exam.SQL
{
    /// <summary>
    /// Data access layer
    /// NOTE: TG.Exam.SQL.sql was also modified!
    /// </summary>
    public class DAL
    {
        /// <summary>
        /// Open new SQL connection
        /// </summary>
        /// <returns>SQL connection</returns>
        private SqlConnection GetConnection() 
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            var con = new SqlConnection(connectionString);

            con.Open();

            return con;
        }

        /// <summary>
        /// Execute SQL query and fill DataSet 
        /// </summary>
        /// <param name="sql">SQL query</param>
        /// <returns>DataSet with data from DB</returns>
        private DataSet GetData(string sql)
        {
            var ds = new DataSet();

            using (var con = GetConnection())
            using (var cmd = new SqlCommand(sql, con))
            using (var adp = new SqlDataAdapter(cmd))
            {
                adp.Fill(ds);
            }

            return ds;
        }

        /// <summary>
        /// Execute SQL query
        /// </summary>
        /// <param name="sql">SQL query</param>
        private void Execute(string sql)
        {
            using (var con = GetConnection())
            using (var cmd = new SqlCommand(sql, con))
            {
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Get all orders from DB
        /// </summary>
        /// <returns>Orders</returns>
        public DataTable GetAllOrders()
        {
            var sql = 
                "SELECT OrderId, OrderCustomerId, OrderDate " +
                "FROM dbo.Orders " +
                "ORDER BY OrderId";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Get all orders with customers
        /// </summary>
        /// <returns>Orders with customers</returns>
        public DataTable GetAllOrdersWithCustomers()
        {
            var sql = 
                "SELECT o.OrderId, o.OrderDate, c.CustomerId, c.CustomerFirstName, c.CustomerLastName " +
                "FROM dbo.Orders o " +
                "JOIN dbo.Customers c on c.CustomerId = o.OrderCustomerId";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Get all orders with price under parameter
        /// </summary>
        /// <param name="price">Price</param>
        /// <returns>Orders with price under parameter</returns>
        public DataTable GetAllOrdersWithPriceUnder(int price)
        {
            var sql =
                "SELECT o.OrderId, o.OrderCustomerId, o.OrderDate, SUM(i.ItemPrice) AS OrderPrice FROM dbo.Orders o " +
                "JOIN dbo.OrdersItems oi ON o.OrderId = oi.OrderId " +
                "JOIN dbo.Items i ON i.ItemId = oi.ItemId " +
                "GROUP BY o.OrderId, o.OrderCustomerId, o.OrderDate " +
                $"HAVING SUM(i.ItemPrice) > {price}";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Delete customer by orderId
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        public void DeleteCustomer(int orderId)
        {
            var sql =
                "DELETE dbo.Customers FROM dbo.Customers c " +
                "JOIN dbo.Orders o ON o.OrderCustomerId = c.CustomerId " +
                $"WHERE o.OrderId = {orderId}";

            Execute(sql);
        }

        /// <summary>
        /// Get all items and their orders count including the items without orders
        /// </summary>
        /// <returns>Select result</returns>
        internal DataTable GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrders()
        {
            var sql = 
                "SELECT i.ItemId, i.ItemName, i.ItemPrice, count(o.OrderId) AS OrdersCount FROM dbo.Items i " +
                "LEFT JOIN dbo.OrdersItems oi ON oi.ItemId = i.ItemId " +
                "LEFT JOIN dbo.Orders o ON oi.OrderId = o.OrderId " +
                "GROUP BY i.ItemId, i.ItemName, i.ItemPrice";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }
    }
}
