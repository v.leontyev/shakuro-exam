﻿using TG.Exam.OOP.Interfaces;

namespace TG.Exam.OOP.DTOs
{
    /// <summary>
    /// Base employee class
    /// </summary>
    public abstract class BaseEmployee : IFormattable2
    {
        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Salary
        /// </summary>
        public int Salary { get; set; }

        /// <inheritdoc />
        public abstract string ToString2();
    }
}