﻿using TG.Exam.OOP.Interfaces;

namespace TG.Exam.OOP.DTOs
{
    /// <summary>
    /// Dog
    /// </summary>
    public class Dog : IFormattable2
    {
        /// <summary>
        /// Name of the dog
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Age of the dog
        /// </summary>
        public int Age { get; set; }

        /// <inheritdoc />
        public string ToString2()
        {
            return $"Name = {Name}; Age = {Age}.";
        }
    }
}