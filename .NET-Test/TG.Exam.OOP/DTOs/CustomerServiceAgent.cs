﻿namespace TG.Exam.OOP.DTOs
{
    /// <summary>
    /// Customer service agent
    /// </summary>
    public class CustomerServiceAgent : Employee
    {
        /// <summary>
        /// Customers amount
        /// </summary>
        public int Customers { get; set; }

        /// <inheritdoc />
        public override string ToString2()
        {
            return $"FirstName = {FirstName}; LastName = {LastName}; Salary = {Salary}; Customers = {Customers}.";
        }
    }
}