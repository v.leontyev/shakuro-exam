﻿namespace TG.Exam.OOP.DTOs
{
    /// <summary>
    /// Employee
    /// </summary>
    public class Employee : BaseEmployee
    {
        /// <inheritdoc />
        public override string ToString2()
        {
            return $"FirstName = {FirstName}; LastName = {LastName}; Salary = {Salary}.";
        }
    }
}