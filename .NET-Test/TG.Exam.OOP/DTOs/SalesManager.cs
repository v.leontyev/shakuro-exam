﻿namespace TG.Exam.OOP.DTOs
{
    /// <summary>
    /// Sales manager
    /// </summary>
    public class SalesManager : Employee
    {
        /// <summary>
        /// Bonus per sale
        /// </summary>
        public int BonusPerSale { get; set; }

        /// <summary>
        /// Sales this month
        /// </summary>
        public int SalesThisMonth { get; set; }

        /// <inheritdoc />
        public override string ToString2()
        {
            return $"FirstName = {FirstName}; LastName = {LastName}; Salary = {Salary}; BonusPerSale = {BonusPerSale}; SalesThisMonth = {SalesThisMonth}.";
        }
    }
}