﻿namespace TG.Exam.OOP.Interfaces
{
    /// <summary>
    /// Provides functionality to format the value of an object into a string representation.
    /// </summary>
    public interface IFormattable2
    {
        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        string ToString2();
    }
}