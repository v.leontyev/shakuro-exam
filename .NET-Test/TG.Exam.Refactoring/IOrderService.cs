﻿using System.ServiceModel;
using System.ServiceModel.Web;
using TG.Exam.Refactoring.Messaging;

namespace TG.Exam.Refactoring
{
    /// <summary>
    /// Order WCF web service
    /// </summary>
    [ServiceContract]
    public interface IOrderService
    {
        /// <summary>
        /// Load order by identifier
        /// </summary>
        /// <param name="orderId">Order identifier</param>
        /// <returns>Order information</returns>
        [OperationContract]
        [WebGet(UriTemplate = "/LoadOrder?orderId={orderId}")]
        Order LoadOrder(int orderId);
    }
}