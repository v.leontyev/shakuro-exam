﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using log4net;
using log4net.Config;
using TG.Exam.Refactoring.Messaging;

namespace TG.Exam.Refactoring
{
    /// <inheritdoc />
    public class OrderService : IOrderService
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(OrderService));

        readonly string connectionString = ConfigurationManager.ConnectionStrings["OrdersDBConnectionString"].ConnectionString;

        private static ConcurrentDictionary<int, Order> cache = new ConcurrentDictionary<int, Order>();

        public OrderService()
        {
            BasicConfigurator.Configure();
        }

        /// <summary>
        /// Load order by identifier
        /// </summary>
        /// <remarks>
        ///     1. The WCF service was configured incorrectly. Need to configure it in order to be able to send get request.
        ///     2. The logger log4net was configured incorrectly. Need to configure it in order to monitor LoadOrder method performance and log exceptions data.
        ///     3. Instead of using string for OrderId variable it's better to use integer type. In a variable of type string we can pass not only integer value. 
        ///   Despite the fact that we can try to parse string value, need to remake it into integer.
        ///     4. The key type of cache dictionary need to be remade into integer also.
        ///     5. Not static cache variable has no sense because in such case it will contains zero elements each time we call WCF method. Need to mark <see cref="OrderService.cache"> as static and private.
        ///     6. Accessing dictionary with critical section can be replaced by ConcurrentDictionary.
        ///     7. TryGetValue functionality can replace checking for an item and getting the item.
        ///     8. Need to remove this from this.connectionString.
        ///     9. An object, which implemets IDisposable interface, should be declared and instantiated in the using statement in order to call Dispose method correctly.
        ///     10. Using two string variables in order to form query should be replaced with string interpolation.
        ///     11. Need to pass exception into logger.Error call in order to save call stack.
        /// 
        /// </remarks>
        /// <param name="orderId">Order identifier</param>
        /// <returns>Order information</returns>
        public Order LoadOrder(int orderId)
        {
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                Order cacheOrder;
                if (cache.TryGetValue(orderId, out cacheOrder))
                {
                    stopWatch.Stop();
                    logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
                    return cacheOrder;
                }

                string query = $"SELECT OrderId, OrderCustomerId, OrderDate FROM dbo.Orders where OrderId='{orderId}'";

                using (SqlConnection connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Order order = new Order
                            {
                                OrderId = (int)reader[0],
                                OrderCustomerId = (int)reader[1],
                                OrderDate = (DateTime)reader[2]
                            };

                            if (!cache.ContainsKey(orderId))
                                cache.TryAdd(orderId, order);

                            stopWatch.Stop();
                            logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
                            return order;
                        }
                    }
                }

                stopWatch.Stop();
                logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);

                return null;
            }
            catch (SqlException ex)
            {
                logger.Error(ex.Message, ex);
                throw new ApplicationException("Error");
            }
        }
    }
}