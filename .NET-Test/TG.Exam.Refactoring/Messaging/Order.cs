﻿using System;
using System.Runtime.Serialization;

namespace TG.Exam.Refactoring.Messaging
{
    /// <summary>
    /// Order
    /// </summary>
    [DataContract]
    public class Order
    {
        /// <summary>
        /// Order identifier
        /// </summary>
        [DataMember]
        public int OrderId { get; set; }

        /// <summary>
        /// Order customer identifier
        /// </summary>
        [DataMember]
        public int OrderCustomerId { get; set; }

        /// <summary>
        /// Order date
        /// </summary>
        [DataMember]
        public DateTime OrderDate { get; set; }
    }
}